/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#include "uevent.h"
#include <linux/futex.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <xmmintrin.h>

struct lock {
	__UEVENT_ALIGN_TO_CACHE int _Atomic state;
};

struct lock *lock_create(void)
{
	struct lock *lock = aligned_alloc(UEVENT_ALIGN, sizeof *lock);
	if (!lock)
		abort();
	lock->state = 0;
	return lock;
}
void lock_acquire(struct lock *lock)
{
	int expected = 0;
	if (atomic_compare_exchange_strong_explicit(
	        &lock->state, &expected, 1, memory_order_acq_rel,
	        memory_order_acquire))
		return;

	if (expected != 2)
		expected = atomic_exchange_explicit(
		    &lock->state, 2, memory_order_acq_rel);

	while (expected != 0) {
		syscall(__NR_futex, &lock->state, FUTEX_WAIT_PRIVATE, 2,
		        NULL, NULL, 0);
		for (size_t ii = 0U; ii < 800U; ++ii) {
			if (atomic_load_explicit(
			        &lock->state, memory_order_acquire) !=
			    2) {
				expected = atomic_exchange_explicit(
				    &lock->state, 2,
				    memory_order_acq_rel);
				if (0 == expected)
					return;
			}
			_mm_pause();
		}
	}
}

void lock_release(struct lock *lock)
{
	if (atomic_fetch_sub_explicit(&lock->state, 1,
	                              memory_order_acq_rel) != 1) {
		atomic_store_explicit(&lock->state, 0,
		                      memory_order_release);
		syscall(__NR_futex, &lock->state, FUTEX_WAKE_PRIVATE, 1,
		        NULL, NULL, 0);
	}
}
