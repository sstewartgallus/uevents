/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _GNU_SOURCE 1

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/resource.h>
#include <sys/time.h>

#include "test.h"
#include "uevent.h"

#define NUM_THREADS 10U

static int priorities[NUM_THREADS];

static void *print(void *arg);

static struct uevent the_event;
static struct uevent printed;
int main()
{
	test_setup();

	uevent_init(&the_event);
	uevent_init(&printed);

	pthread_t threads[NUM_THREADS];
	for (size_t ii = 0U; ii < NUM_THREADS; ++ii) {
		priorities[ii] = ii;
		pthread_create(&threads[ii], NULL, print,
		               &priorities[ii]);
	}

	/* A hack but this is only a test anyway */
	for (;;) {
		if (NUM_THREADS == uevent_waiter_count(&the_event))
			break;
		sched_yield();
	}

	for (size_t ii = 0U; ii < NUM_THREADS; ++ii) {
		uevent_signal(&the_event);
		uevent_wait(&printed);
	}

	for (size_t ii = 0U; ii < NUM_THREADS; ++ii) {
		void *retval;
		pthread_join(threads[ii], &retval);
	}
	return 0;
}

static void *print(void *arg)
{
	int *priority = arg;
	test_setup();

	setpriority(PRIO_PROCESS, 0, *priority);

	uevent_wait(&the_event);

	fprintf(stderr, "prio: %i\n", *priority);

	uevent_signal(&printed);

	return 0;
}
