/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _GNU_SOURCE 1

#include <errno.h>
#include <pthread.h>
#include <stdio.h>

#include "test.h"
#include "uevent.h"

#define PRODUCER_LOOP 200000000U
#define NUM_SIGNALERS 20U
#define NUM_WAITERS 20U

static void *do_signal(void *arg);
static void *do_wait(void *arg);

static struct uevent the_event;

int main()
{
	test_setup();

	uevent_init(&the_event);

	pthread_t waiters[NUM_WAITERS];
	for (size_t ii = 0U; ii < NUM_WAITERS; ++ii) {
		pthread_create(&waiters[ii], NULL, do_wait, NULL);
	}

	pthread_t signalers[NUM_SIGNALERS];
	for (size_t ii = 0U; ii < NUM_SIGNALERS; ++ii) {
		pthread_create(&signalers[ii], NULL, do_signal, NULL);
	}

	for (size_t ii = 0U; ii < NUM_SIGNALERS; ++ii) {
		void *retval;
		pthread_join(signalers[ii], &retval);
	}
	return 0;
}

static void *do_signal(void *arg)
{
	test_setup();

	for (size_t ii = 0U; ii < PRODUCER_LOOP; ++ii) {
		uevent_signal(&the_event);
	}
	return 0;
}

static void *do_wait(void *arg)
{
	test_setup();

	for (;;) {
		uevent_wait(&the_event);
	}
}
