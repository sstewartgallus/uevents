/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef UEVENT_H
#define UEVENT_H

#include <stdatomic.h>
#include <stddef.h>
#include <stdint.h>

/* Official documentation is that cachelines are 64 bytes but oddly
 * the computer goes faster if there's padding here.
 */

#define UEVENT_ALIGN 64U
#define __UEVENT_ALIGN_TO_CACHE _Alignas(UEVENT_ALIGN)

struct __uevent_node;

struct __uevent_outbox {
	size_t capacity;
	size_t size;
	struct __uevent_node **elems;
};

struct uevent {
	/* is_worker locks access to this */
	struct __uevent_outbox outbox;
	__UEVENT_ALIGN_TO_CACHE struct __uevent_node *_Atomic inbox;
	/* The data is stored here in a little complicated way.
	 * 1 is the starting default state and means there are zero
	 * waiters.
	 * 0 happens when a signal occurs without a waiter.
	 * >1 is when there are waiters waiting on the event.
	 */
	__UEVENT_ALIGN_TO_CACHE atomic_uint_fast32_t waiter_count;
	__UEVENT_ALIGN_TO_CACHE atomic_uint_fast32_t wake_count;
	__UEVENT_ALIGN_TO_CACHE _Bool _Atomic is_worker;
};

void uevent_init(struct uevent *event);
void uevent_wait(struct uevent *event);
void uevent_signal(struct uevent *event);

size_t uevent_waiter_count(struct uevent *event);

/**
 * @precondition Results are undefined (in practise this means an
 * abort) if other threads are waiting or signalling this event when
 * it is destroyed.
 */
void uevent_destroy(struct uevent *event);
#endif
