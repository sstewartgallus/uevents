/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef UEVENT_NOTIFIER_H
#error "notifier-pipe.h should only be included by notifier.h"
#endif

struct notifier_impl;

static struct notifier_aba notifier_aba_make(struct notifier_impl *ptr,
                                             uint32_t tag);
static struct notifier_impl *notifier_aba_ptr(struct notifier_aba aba);
static uint32_t notifier_aba_tag(struct notifier_aba aba);

struct notifier_aba {
	uintptr_t __ptr : 42U;
	uintptr_t __tag : 22U;
};

struct pipefds {
	int readfd;
	int writefd;
};

struct notifier {
	struct notifier_impl *impl;
};

struct notifier_impl {
	struct pipefds pipefds;
	__UEVENT_ALIGN_TO_CACHE atomic_int triggered;
	__UEVENT_ALIGN_TO_CACHE struct notifier_impl *next;
};

typedef _Atomic(struct notifier_aba) atomic_notifier_aba;
static __UEVENT_ALIGN_TO_CACHE atomic_notifier_aba notifier_free_list;

static void notifier_init(struct notifier *notifierp)
{
	struct notifier_impl *head_ptr;
	uint32_t head_tag;
	struct notifier_impl *next;

	{
		struct notifier_aba head;
		head = atomic_load_explicit(&notifier_free_list,
		                            memory_order_acquire);
		head_ptr = notifier_aba_ptr(head);
		head_tag = notifier_aba_tag(head);
	}

	for (;;) {
		if (__builtin_expect(!head_ptr, 0)) {
			struct notifier_impl *n;
			{
				void *xx;
				posix_memalign(&xx, UEVENT_ALIGN,
				               sizeof *n);
				n = xx;
			}
			if (!n)
				abort();
#if defined UEVENT_USE_EVENTFD
			struct pipefds pipefds = {-1, -1};
			{
				int xx = eventfd(0U, EFD_CLOEXEC |
				                         EFD_NONBLOCK);
				if (-1 == xx)
					abort();
				pipefds.readfd = xx;
				pipefds.writefd = xx;

				n->pipefds = pipefds;
			}
#else
			struct pipefds pipefds = {-1, -1};
			{
				int xx[2U];
				if (-1 ==
				    pipe2(xx, O_CLOEXEC | O_NONBLOCK))
					abort();
				pipefds.readfd = xx[0U];
				pipefds.writefd = xx[1U];

				n->pipefds = pipefds;
			}
#endif
			n->next = NULL;
			atomic_store_explicit(&n->triggered, false,
			                      memory_order_release);
			notifierp->impl = n;
			return;
		}

		next = head_ptr->next;

		struct notifier_aba new =
		    notifier_aba_make(next, head_tag + 1U);
		struct notifier_aba head_copy =
		    notifier_aba_make(head_ptr, head_tag);
		if (atomic_compare_exchange_weak_explicit(
		        &notifier_free_list, &head_copy, new,
		        memory_order_acq_rel, memory_order_acquire))
			break;
		head_ptr = notifier_aba_ptr(head_copy);
		head_tag = notifier_aba_tag(head_copy);

		_mm_pause();
	}

	head_ptr->next = NULL;
	atomic_store_explicit(&head_ptr->triggered, false,
	                      memory_order_release);
	notifierp->impl = head_ptr;
}

static void notifier_destroy(struct notifier *notifier)
{
	struct notifier_impl *impl = notifier->impl;
	notifier->impl = NULL;

	struct notifier_impl *head_ptr;
	uint32_t head_tag;

	impl->next = NULL;
	atomic_store_explicit(&impl->triggered, false,
	                      memory_order_release);

	{
		struct notifier_aba head;
		head = atomic_load_explicit(&notifier_free_list,
		                            memory_order_acquire);
		head_ptr = notifier_aba_ptr(head);
		head_tag = notifier_aba_tag(head);
	}

	for (;;) {
		impl->next = head_ptr;

		struct notifier_aba new =
		    notifier_aba_make(impl, head_tag + 1U);
		struct notifier_aba head_copy =
		    notifier_aba_make(head_ptr, head_tag);
		if (atomic_compare_exchange_weak_explicit(
		        &notifier_free_list, &head_copy, new,
		        memory_order_acq_rel, memory_order_acquire))
			break;

		head_ptr = notifier_aba_ptr(head_copy);
		head_tag = notifier_aba_tag(head_copy);

		_mm_pause();
	}
}

static void notifier_signal(struct notifier *notifier)
{
	struct notifier_impl *impl = notifier->impl;

	int expected = 1;
	if (atomic_compare_exchange_strong_explicit(
	        &impl->triggered, &expected, 2, memory_order_acq_rel,
	        memory_order_acquire))
		return;
	atomic_store_explicit(&impl->triggered, 2,
	                      memory_order_release);

	struct pipefds pipefds = impl->pipefds;
	{
		static uint64_t const one = 1U;
		if (-1 == write(pipefds.writefd, &one, sizeof one)) {
			if (errno != EAGAIN)
				abort();
		}
	}
}

static void notifier_wait(struct notifier *notifier)
{
	struct notifier_impl *impl = notifier->impl;

	for (;;) {
		int expected = 0;
		atomic_compare_exchange_strong_explicit(
		    &impl->triggered, &expected, 1,
		    memory_order_acq_rel, memory_order_acquire);

		for (size_t ii = 0U; ii < NOTIFIER_SPIN_LOOPS; ++ii) {
			int expected = 2;
			if (atomic_compare_exchange_strong_explicit(
			        &impl->triggered, &expected, 0,
			        memory_order_acq_rel,
			        memory_order_acquire))
				return;
			_mm_pause();
		}
		expected = 1;
		if (!atomic_compare_exchange_strong_explicit(
		        &impl->triggered, &expected, 0,
		        memory_order_acq_rel, memory_order_acquire))
			continue;

		int readfd = impl->pipefds.readfd;
		{
			struct pollfd fds[1U] = {
			    {.fd = readfd, .events = POLLIN}};
			if (-1 == poll(fds, 1U, -1)) {
				abort();
			}
		}

		for (;;) {
			uint64_t xx;
			if (-1 == read(readfd, &xx, sizeof xx)) {
				if (EAGAIN == errno)
					break;
				abort();
			}
		}
	}
}

static struct notifier_aba notifier_aba_make(struct notifier_impl *ptr,
                                             uint32_t tag)
{
	uintptr_t ptr_val = (uintptr_t)ptr;
	assert(((uintptr_t)ptr_val & ((1 << 6U) - 1U)) == 0);
	return (struct notifier_aba){ptr_val >> 6U, tag};
}

static struct notifier_impl *notifier_aba_ptr(struct notifier_aba aba)
{
	uintptr_t ptr = aba.__ptr;
	ptr = ptr << 6U;
	return (void *)ptr;
}

static uint32_t notifier_aba_tag(struct notifier_aba aba)
{
	return aba.__tag;
}
