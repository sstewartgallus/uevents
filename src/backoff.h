/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef UEVENT_BACKOFF_H
#define UEVENT_BACKOFF_H

typedef uint32_t backoff_counter;

#define BACKOFF_INIT 0U

static inline backoff_counter backoff(backoff_counter counter);
static inline _Bool backoff_fail(backoff_counter *counter);

static backoff_counter backoff(backoff_counter counter)
{
	if (counter > 24U) {
		sched_yield();
		return counter;
	}

	uint32_t max = 5U;
	uint32_t loops = 1U << (counter < max ? counter : max);

	for (uint32_t ii = 0U; ii < loops; ++ii)
		_mm_pause();

	return counter + 1U;
}

static _Bool backoff_fail(backoff_counter *counter)
{
	backoff_counter counter_value = *counter;

	if (counter_value >= 4U)
		return true;

	uint32_t loops = (1U << counter_value);
	for (uint32_t ii = 0U; ii < loops; ++ii)
		_mm_pause();

	*counter = counter_value + 1U;
	return false;
}

#endif
