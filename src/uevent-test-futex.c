/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _GNU_SOURCE 1

#include "uevent.h"

#include <linux/futex.h>
#include <stdatomic.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <xmmintrin.h>

/* A dummy futex implementation */

void uevent_init(struct uevent *event)
{
	atomic_store_explicit(&event->waiter_count, 0,
	                      memory_order_relaxed);
	atomic_store_explicit(&event->wake_count, 0,
	                      memory_order_relaxed);
}

void uevent_wait(struct uevent *event)
{
	int_fast32_t count = atomic_fetch_add_explicit(
	    &event->waiter_count, -1, memory_order_acq_rel);
	--count;
	if (count >= 0) {
		return;
	}

	uint_fast32_t wake_count = atomic_load_explicit(
	    &event->wake_count, memory_order_acquire);
	for (;;) {
		if (0U == wake_count) {
			syscall(__NR_futex, &event->wake_count,
			        FUTEX_WAIT_PRIVATE, 0, NULL, NULL, 0);
			wake_count = atomic_load_explicit(
			    &event->wake_count, memory_order_acquire);
			continue;
		}
		if (atomic_compare_exchange_weak_explicit(
		        &event->wake_count, &wake_count,
		        wake_count - 1U, memory_order_acq_rel,
		        memory_order_acquire))
			break;
		_mm_pause();
	}
}

void uevent_signal(struct uevent *event)
{
	int_fast32_t count = atomic_load_explicit(&event->waiter_count,
	                                          memory_order_acquire);
	for (;;) {
		if (count >= 1)
			return;

		if (atomic_compare_exchange_weak_explicit(
		        &event->waiter_count, &count, count + 1,
		        memory_order_acq_rel, memory_order_acquire)) {
			++count;
			break;
		}
		_mm_pause();
	}
	if (count > 0)
		return;

	atomic_fetch_add_explicit(&event->wake_count, 1U,
	                          memory_order_release);
	syscall(__NR_futex, &event->wake_count, FUTEX_WAKE_PRIVATE, 1,
	        NULL, NULL, 0);
}

void uevent_destroy(struct uevent *event)
{
}
