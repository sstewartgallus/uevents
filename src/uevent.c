/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _GNU_SOURCE 1

#include "uevent.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>
#include <xmmintrin.h>

#if defined UEVENT_USE_EVENTFD
#include <sys/eventfd.h>
#endif

#if defined UEVENT_USE_FUTEX
#include <linux/futex.h>
#include <sys/syscall.h>
#endif

#include "node.h"
#include "notifier.h"

static void flush_pending_signals(struct uevent *event);

void uevent_init(struct uevent *event)
{
	atomic_store_explicit(&event->inbox, NULL,
	                      memory_order_relaxed);
	event->outbox.elems = NULL;
	event->outbox.size = 0U;
	atomic_store_explicit(&event->waiter_count, 1U,
	                      memory_order_relaxed);
	atomic_store_explicit(&event->wake_count, 0,
	                      memory_order_relaxed);
	atomic_store_explicit(&event->is_worker, 0,
	                      memory_order_relaxed);
}

void uevent_wait(struct uevent *event)
{
	node the_node = {0};
	the_node.priority = getpriority(PRIO_PROCESS, 0) + 20U;
	notifier_init(&the_node.trigger);

	node_enqueue(event, &the_node);

	uint_fast32_t old_count = atomic_fetch_add_explicit(
	    &event->waiter_count, 1U, memory_order_acquire);
	if (0U == old_count) {
		atomic_fetch_add_explicit(&event->wake_count, 1,
		                          memory_order_release);
	}

	flush_pending_signals(event);

	notifier_wait(&the_node.trigger);

	notifier_destroy(&the_node.trigger);
}

void uevent_signal(struct uevent *event)
{
	uint_fast32_t count = atomic_load_explicit(
	    &event->waiter_count, memory_order_acquire);
	backoff_counter back_counter = BACKOFF_INIT;
	for (;;) {
		if (count == 0U)
			break;

		if (atomic_compare_exchange_weak_explicit(
		        &event->waiter_count, &count, count - 1U,
		        memory_order_acq_rel, memory_order_acquire)) {
			--count;
			break;
		}
		back_counter = backoff(back_counter);
	}
	if (count == 0U)
		return;

	atomic_fetch_add_explicit(&event->wake_count, 1,
	                          memory_order_release);
	flush_pending_signals(event);
}

void uevent_destroy(struct uevent *event)
{
	free(event->outbox.elems);
}

size_t uevent_waiter_count(struct uevent *event)
{
	uint_fast32_t count = atomic_load_explicit(
	    &event->waiter_count, memory_order_acquire);
	if (0U == count)
		return 0U;
	return count - 1U;
}

static void flush_pending_signals(struct uevent *event)
{
restart_work:;
	_Bool expected = false;
	if (!atomic_compare_exchange_strong_explicit(
	        &event->is_worker, &expected, true,
	        memory_order_acq_rel, memory_order_acquire))
		return;

	uint_fast32_t wake_count = atomic_load_explicit(
	    &event->wake_count, memory_order_acquire);

	while (wake_count > 0U) {
		for (backoff_counter back_counter = BACKOFF_INIT;;) {
			if (atomic_compare_exchange_weak_explicit(
			        &event->wake_count, &wake_count,
			        wake_count - 1U, memory_order_acq_rel,
			        memory_order_acquire)) {
				--wake_count;
				break;
			}
			if (0U == wake_count)
				goto finish_work;
			back_counter = backoff(back_counter);
		}

		node *node = node_dequeue(event);
		assert(node != NULL);

		notifier_signal(&node->trigger);
	}
finish_work:
	atomic_store_explicit(&event->is_worker, false,
	                      memory_order_release);
	wake_count = atomic_load_explicit(&event->wake_count,
	                                  memory_order_acquire);
	if (wake_count > 0U)
		goto restart_work;
}
